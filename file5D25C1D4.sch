EESchema Schematic File Version 4
LIBS:battery_emulator-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 4750 4800 0    50   Input ~ 0
SDA
Text HLabel 4750 4650 0    50   Input ~ 0
SCl
Text HLabel 7250 4650 2    50   Input ~ 0
VSS
$Comp
L battery_emulator-rescue:MCP4726-battery_emulator U3
U 1 1 5D274596
P 5900 4650
F 0 "U3" H 5900 5065 50  0000 C CNN
F 1 "MCP4726" H 5900 4974 50  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-23-6" H 5800 4950 50  0001 C CNN
F 3 "" H 5900 4650 50  0001 C CNN
F 4 "MCP4726A0T-E/CHCT-ND" H 6000 5150 50  0001 C CNN "Digikey Part #"
	1    5900 4650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6350 4500 6500 4500
$Comp
L Device:C C5
U 1 1 5D274BE9
P 6850 4350
F 0 "C5" H 6965 4396 50  0000 L CNN
F 1 "0.1uF(DNP)" H 6965 4305 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 6888 4200 50  0001 C CNN
F 3 "~" H 6850 4350 50  0001 C CNN
	1    6850 4350
	-1   0    0    -1  
$EndComp
Text Notes 3050 5150 0    50   ~ 0
40 mA max sink/source on VOUT
Text Notes 6050 5000 2    50   ~ 0
12-bit
Text Notes 3050 4350 0    50   ~ 0
Set VREF to buffered mode for higher input impedance
Text HLabel 3500 4500 0    50   Input ~ 0
VREF
Wire Wire Line
	5450 4650 4750 4650
Wire Wire Line
	5450 4800 4750 4800
$Comp
L battery_emulator-rescue:MAX8515-battery_emulator U2
U 1 1 5D301FFB
P 5900 3400
F 0 "U2" H 5900 3035 50  0000 C CNN
F 1 "MAX8515" H 5900 3126 50  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-23-5" H 5800 3650 50  0001 C CNN
F 3 "" H 5900 3750 50  0001 C CNN
F 4 "MAX8515AEZK+TCT-ND" H 6000 3850 50  0001 C CNN "Digikey Part #"
	1    5900 3400
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 5D302118
P 6700 3300
AR Path="/5D302118" Ref="R?"  Part="1" 
AR Path="/5D25C1D5/5D302118" Ref="R13"  Part="1" 
F 0 "R13" H 6850 3400 50  0000 C CNN
F 1 "10k" H 6850 3300 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 6630 3300 50  0001 C CNN
F 3 "~" H 6700 3300 50  0001 C CNN
F 4 "RNCP0603FTD10K0CT-ND" H 6700 3300 50  0001 C CNN "Digikey Part #"
	1    6700 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5D302380
P 6700 3700
AR Path="/5D302380" Ref="R?"  Part="1" 
AR Path="/5D25C1D5/5D302380" Ref="R15"  Part="1" 
F 0 "R15" H 6550 3750 50  0000 C CNN
F 1 "63.4k" H 6500 3650 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 6630 3700 50  0001 C CNN
F 3 "~" H 6700 3700 50  0001 C CNN
F 4 "RMCF0603FT3K48CT-ND" H 6700 3700 50  0001 C CNN "Digikey Part #"
	1    6700 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5D3024D1
P 6950 3500
AR Path="/5D3024D1" Ref="R?"  Part="1" 
AR Path="/5D25C1D5/5D3024D1" Ref="R14"  Part="1" 
F 0 "R14" V 7050 3500 50  0000 C CNN
F 1 "390k" V 7150 3500 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 6880 3500 50  0001 C CNN
F 3 "~" H 6950 3500 50  0001 C CNN
F 4 "311-390KHRCT-ND" V 6950 3500 50  0001 C CNN "Digikey Part #"
	1    6950 3500
	0    1    1    0   
$EndComp
Wire Wire Line
	6700 3450 6700 3500
Connection ~ 6700 3500
Wire Wire Line
	6700 3500 6800 3500
Wire Wire Line
	6700 3550 6700 3500
Text Notes 7450 3400 0    50   ~ 0
Control voltage here from 0-3.798V approx
Text HLabel 5400 3400 0    50   Input ~ 0
VSS
$Comp
L Device:C C4
U 1 1 5D303134
P 4800 3250
F 0 "C4" H 4915 3296 50  0000 L CNN
F 1 "0.22uF" H 4915 3205 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 4838 3100 50  0001 C CNN
F 3 "~" H 4800 3250 50  0001 C CNN
	1    4800 3250
	1    0    0    1   
$EndComp
Wire Wire Line
	5450 3300 5350 3300
Wire Wire Line
	5350 3300 5350 3050
Text HLabel 6750 3900 2    50   Input ~ 0
VSS
Wire Wire Line
	6700 3850 6700 3900
Wire Wire Line
	6700 3900 6750 3900
$Comp
L Device:R R?
U 1 1 5D30B13D
P 4800 2850
AR Path="/5D30B13D" Ref="R?"  Part="1" 
AR Path="/5D25C1D5/5D30B13D" Ref="R11"  Part="1" 
F 0 "R11" H 4950 2900 50  0000 C CNN
F 1 "5.1k" H 4950 2800 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 4730 2850 50  0001 C CNN
F 3 "~" H 4800 2850 50  0001 C CNN
	1    4800 2850
	1    0    0    1   
$EndComp
Wire Wire Line
	6350 3500 6700 3500
Wire Wire Line
	6350 3300 6600 3300
Wire Wire Line
	4800 3050 5350 3050
Wire Wire Line
	4800 3050 4800 3000
$Comp
L Device:Q_PNP_BCE Q2
U 1 1 5D3131D8
P 4550 2600
F 0 "Q2" H 4741 2554 50  0000 L CNN
F 1 "Q_PNP_BCE" H 4741 2645 50  0000 L CNN
F 2 "battery_emulator:TIP32C" H 4750 2700 50  0001 C CNN
F 3 "~" H 4550 2600 50  0001 C CNN
F 4 "497-2628-5-ND" H 4550 2600 50  0001 C CNN "Digikey Part #"
	1    4550 2600
	-1   0    0    1   
$EndComp
$Comp
L Device:Q_NPN_BCE Q3
U 1 1 5D31329F
P 3850 2800
F 0 "Q3" H 4040 2846 50  0000 L CNN
F 1 "Q_NPN_BCE" H 4040 2755 50  0000 L CNN
F 2 "battery_emulator:TIP35C" H 4050 2900 50  0001 C CNN
F 3 "~" H 3850 2800 50  0001 C CNN
F 4 "497-2609-5-ND" H 3850 2800 50  0001 C CNN "Digikey Part #"
	1    3850 2800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4800 2700 4800 2600
Wire Wire Line
	4800 2600 4750 2600
Wire Wire Line
	4050 2800 4450 2800
$Comp
L Device:R R?
U 1 1 5D316A55
P 4450 2950
AR Path="/5D316A55" Ref="R?"  Part="1" 
AR Path="/5D25C1D5/5D316A55" Ref="R12"  Part="1" 
F 0 "R12" H 4600 3000 50  0000 C CNN
F 1 "470" H 4600 2900 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 4380 2950 50  0001 C CNN
F 3 "~" H 4450 2950 50  0001 C CNN
	1    4450 2950
	1    0    0    1   
$EndComp
Connection ~ 4450 2800
Wire Wire Line
	3750 3000 3750 3100
Wire Wire Line
	3750 3100 4450 3100
Connection ~ 3750 3100
Wire Wire Line
	4800 3100 4800 3050
Connection ~ 4800 3050
Wire Wire Line
	3750 2600 3750 2350
Wire Wire Line
	3750 2350 4450 2350
Wire Wire Line
	4450 2350 4450 2400
Wire Wire Line
	6600 2350 6600 3300
Text HLabel 3700 3100 0    50   Input ~ 0
PGND
Wire Wire Line
	3700 3100 3750 3100
Wire Wire Line
	3750 3100 3750 3500
Wire Wire Line
	3750 3500 4800 3500
Text Notes 3100 2250 0    50   ~ 0
High power transistors\n
Wire Wire Line
	5400 3400 5450 3400
Wire Wire Line
	4800 3400 4800 3500
Connection ~ 4800 3500
Wire Wire Line
	4800 3500 5450 3500
$Comp
L Device:R R?
U 1 1 5D329DD5
P 5900 2350
AR Path="/5D329DD5" Ref="R?"  Part="1" 
AR Path="/5D25C1D5/5D329DD5" Ref="R10"  Part="1" 
F 0 "R10" V 5700 2350 50  0000 C CNN
F 1 "46.4" V 5800 2350 50  0000 C CNN
F 2 "Resistors_SMD:R_1210" V 5830 2350 50  0001 C CNN
F 3 "~" H 5900 2350 50  0001 C CNN
F 4 "541-46.4AACT-ND" V 5900 2350 50  0001 C CNN "Digikey Part #"
	1    5900 2350
	0    -1   1    0   
$EndComp
Connection ~ 6600 2350
Wire Wire Line
	5750 2350 4800 2350
Connection ~ 4450 2350
Wire Wire Line
	6050 2350 6600 2350
Text HLabel 3700 2350 0    50   Input ~ 0
VOUT
Wire Wire Line
	3700 2350 3750 2350
Connection ~ 3750 2350
Text HLabel 6750 2900 2    50   Input ~ 0
VOUT
Wire Wire Line
	6700 3150 6700 2900
Wire Wire Line
	6700 2900 6750 2900
Wire Notes Line
	3100 2300 3100 3150
Wire Notes Line
	3100 3150 4700 3150
Wire Notes Line
	4700 3150 4700 2300
Wire Notes Line
	4700 2300 3100 2300
Text Notes 3100 3250 0    50   ~ 0
Darlington
Wire Wire Line
	6850 4500 6850 4650
Wire Wire Line
	6850 4150 6850 4200
Wire Wire Line
	6500 4150 6500 4500
Text Notes 9950 2950 0    50   ~ 0
USB 4.4-5.25V
Wire Wire Line
	7100 3500 7600 3500
Wire Wire Line
	7600 4150 7600 3500
$Comp
L Device:C C6
U 1 1 5D3458CD
P 3850 4650
F 0 "C6" H 3965 4696 50  0000 L CNN
F 1 "0.1uF(DNP)" H 3965 4605 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 3888 4500 50  0001 C CNN
F 3 "~" H 3850 4650 50  0001 C CNN
	1    3850 4650
	-1   0    0    -1  
$EndComp
Connection ~ 6850 4150
Wire Wire Line
	6850 4150 7600 4150
Connection ~ 6850 4650
Wire Wire Line
	6350 4650 6850 4650
Wire Wire Line
	6500 4150 6850 4150
$Comp
L Device:C C7
U 1 1 5D346D56
P 4300 4650
F 0 "C7" H 4415 4696 50  0000 L CNN
F 1 "10uF(DNP)" H 4415 4605 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 4338 4500 50  0001 C CNN
F 3 "~" H 4300 4650 50  0001 C CNN
	1    4300 4650
	-1   0    0    -1  
$EndComp
Connection ~ 3850 4500
Wire Wire Line
	3850 4500 3500 4500
Wire Wire Line
	3850 4500 4300 4500
Wire Wire Line
	4300 4500 5450 4500
Connection ~ 4300 4500
Text HLabel 3500 4800 0    50   Input ~ 0
VSS
Wire Wire Line
	3500 4800 3850 4800
Wire Wire Line
	3850 4800 4300 4800
Connection ~ 3850 4800
Wire Notes Line
	3000 4100 3000 5200
Wire Notes Line
	3000 5200 7950 5200
Wire Notes Line
	7950 5200 7950 4100
Wire Notes Line
	7950 4100 3000 4100
Text Notes 3050 5300 0    50   ~ 0
Digital Interface
Text Notes 5600 3700 0    50   ~ 0
Shunt Regulator
Text Notes 5200 2050 0    50   ~ 0
Limits to 0.5W at max drop (100mA)
Text HLabel 10200 3000 2    50   Input ~ 0
VDD
$Comp
L Device:C C3
U 1 1 5D3BF084
P 10150 3150
F 0 "C3" H 10265 3196 50  0000 L CNN
F 1 "10uF" H 10265 3105 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 10188 3000 50  0001 C CNN
F 3 "~" H 10150 3150 50  0001 C CNN
	1    10150 3150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10150 3000 10200 3000
$Comp
L Device:C C2
U 1 1 5D3BF08C
P 9750 3150
F 0 "C2" H 9865 3196 50  0000 L CNN
F 1 "0.1uF" H 9865 3105 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 9788 3000 50  0001 C CNN
F 3 "~" H 9750 3150 50  0001 C CNN
	1    9750 3150
	-1   0    0    -1  
$EndComp
Connection ~ 10150 3000
Wire Wire Line
	9750 3000 10150 3000
Connection ~ 9750 3000
Wire Wire Line
	9300 3000 9750 3000
Wire Wire Line
	9300 2350 9300 3000
Connection ~ 9300 3000
Wire Wire Line
	6600 2350 9300 2350
Text HLabel 10200 3300 2    50   Input ~ 0
VSS
Wire Wire Line
	10150 3300 10200 3300
Connection ~ 10150 3300
Wire Wire Line
	9750 3300 10150 3300
Wire Wire Line
	6850 4650 7250 4650
Wire Wire Line
	9300 4800 6350 4800
Wire Wire Line
	9300 3000 9300 4800
$Comp
L Connector:TestPoint TP6
U 1 1 5D29A732
P 4800 2350
F 0 "TP6" H 4858 2470 50  0000 L CNN
F 1 "TestPoint" H 4858 2379 50  0000 L CNN
F 2 "TestPoint:TestPoint_THTPad_2.0x2.0mm_Drill1.0mm" H 5000 2350 50  0001 C CNN
F 3 "~" H 5000 2350 50  0001 C CNN
	1    4800 2350
	1    0    0    -1  
$EndComp
Connection ~ 4800 2350
Wire Wire Line
	4800 2350 4450 2350
$EndSCHEMATC
