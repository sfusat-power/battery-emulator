EESchema Schematic File Version 4
LIBS:battery_emulator-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R1
U 1 1 5D25B413
P 6550 3450
F 0 "R1" V 6343 3450 50  0000 C CNN
F 1 "ESR" V 6434 3450 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 6480 3450 50  0001 C CNN
F 3 "~" H 6550 3450 50  0001 C CNN
	1    6550 3450
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x02_Male J1
U 1 1 5D25BE96
P 6150 2200
F 0 "J1" H 6256 2378 50  0000 C CNN
F 1 "SUPPLY" H 6256 2287 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 6150 2200 50  0001 C CNN
F 3 "~" H 6150 2200 50  0001 C CNN
	1    6150 2200
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J2
U 1 1 5D25BED6
P 3300 3450
F 0 "J2" H 3406 3628 50  0000 C CNN
F 1 "TERMINALS" H 3406 3537 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 3300 3450 50  0001 C CNN
F 3 "~" H 3300 3450 50  0001 C CNN
	1    3300 3450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5D25C091
P 3700 4800
F 0 "#PWR0101" H 3700 4550 50  0001 C CNN
F 1 "GND" H 3705 4627 50  0000 C CNN
F 2 "" H 3700 4800 50  0001 C CNN
F 3 "" H 3700 4800 50  0001 C CNN
	1    3700 4800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5D25C0A9
P 6350 2300
F 0 "#PWR0102" H 6350 2050 50  0001 C CNN
F 1 "GND" H 6355 2127 50  0000 C CNN
F 2 "" H 6350 2300 50  0001 C CNN
F 3 "" H 6350 2300 50  0001 C CNN
	1    6350 2300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5D25C165
P 7300 5050
F 0 "#PWR0103" H 7300 4800 50  0001 C CNN
F 1 "GND" H 7305 4877 50  0000 C CNN
F 2 "" H 7300 5050 50  0001 C CNN
F 3 "" H 7300 5050 50  0001 C CNN
	1    7300 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 4050 7400 4050
Wire Wire Line
	6550 2200 6350 2200
Wire Wire Line
	6700 3450 7300 3450
$Comp
L Device:Fuse F1
U 1 1 5D26280D
P 6550 2350
F 0 "F1" H 6610 2396 50  0000 L CNN
F 1 "1.5A Polyfuse" H 6610 2305 50  0000 L CNN
F 2 "battery_emulator:0ZCJ0150FF2C" V 6480 2350 50  0001 C CNN
F 3 "~" H 6550 2350 50  0001 C CNN
F 4 "507-1808-1-ND" H 6550 2350 50  0001 C CNN "Digikey Part #"
	1    6550 2350
	1    0    0    -1  
$EndComp
Connection ~ 6550 2200
$Comp
L power:GND #PWR0104
U 1 1 5D26729B
P 7300 2700
F 0 "#PWR0104" H 7300 2450 50  0001 C CNN
F 1 "GND" H 7305 2527 50  0000 C CNN
F 2 "" H 7300 2700 50  0001 C CNN
F 3 "" H 7300 2700 50  0001 C CNN
	1    7300 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:D_TVS D1
U 1 1 5D267F62
P 7300 2350
F 0 "D1" V 7254 2429 50  0000 L CNN
F 1 "D_TVS" V 7345 2429 50  0000 L CNN
F 2 "battery_emulator:DF2B7AE" H 7300 2350 50  0001 C CNN
F 3 "~" H 7300 2350 50  0001 C CNN
F 4 "DF2B7AEH3FCT-ND" V 7300 2350 50  0001 C CNN "Digikey Part #"
	1    7300 2350
	0    1    1    0   
$EndComp
Wire Wire Line
	7300 2700 7300 2500
Connection ~ 7300 2700
Connection ~ 7300 2200
$Comp
L Device:C C1
U 1 1 5D26B07E
P 7850 2350
F 0 "C1" H 7965 2396 50  0000 L CNN
F 1 "0.1uF" H 7965 2305 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 7888 2200 50  0001 C CNN
F 3 "~" H 7850 2350 50  0001 C CNN
	1    7850 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 2500 7850 2700
Wire Wire Line
	7300 2200 7850 2200
Wire Wire Line
	7300 2700 7850 2700
$Comp
L power:GND #PWR0105
U 1 1 5D276814
P 10150 4600
F 0 "#PWR0105" H 10150 4350 50  0001 C CNN
F 1 "GND" H 10155 4427 50  0000 C CNN
F 2 "" H 10150 4600 50  0001 C CNN
F 3 "" H 10150 4600 50  0001 C CNN
	1    10150 4600
	1    0    0    -1  
$EndComp
Connection ~ 3700 4500
Text Notes 5950 3600 0    50   ~ 0
Subtract current sense and trace resistance from this
Text Notes 1250 2100 0    50   ~ 0
Single vref on board, <=4.4V, needs to be high enough for high-SoC battery sensing/emulation
Text Notes 4550 1750 0    50   ~ 0
USB 4.4-5.25V\nmassive 12% error, not a good vref
Text Notes 1550 1050 0    50   ~ 0
About $15 worth of IC parts not including passives
Text Notes 4150 4850 0    50   ~ 0
How to combine to 2S config?
Text Notes 4000 4950 0    50   ~ 0
Up the external voltage sense range
$Comp
L battery_emulator-rescue:LM4041-battery_emulator U1
U 1 1 5D268925
P 1500 3900
F 0 "U1" H 1500 3585 50  0000 C CNN
F 1 "LM4041" H 1500 3676 50  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 1400 4100 50  0001 C CNN
F 3 "" H 1500 3900 50  0001 C CNN
F 4 "576-1049-1-ND" H 1600 4300 50  0001 C CNN "Digikey Part #"
	1    1500 3900
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 5D269A0F
P 2050 3850
AR Path="/5D25C1D5/5D269A0F" Ref="R?"  Part="1" 
AR Path="/5D269A0F" Ref="R4"  Part="1" 
F 0 "R4" H 2120 3896 50  0000 L CNN
F 1 "100" H 2120 3805 50  0000 L CNN
F 2 "Resistors_SMD:R_0603" V 1980 3850 50  0001 C CNN
F 3 "~" H 2050 3850 50  0001 C CNN
F 4 "P100DBCT-ND" H 2050 3850 50  0001 C CNN "Digikey Part #"
	1    2050 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5D26D591
P 2050 4150
AR Path="/5D25C1D5/5D26D591" Ref="R?"  Part="1" 
AR Path="/5D26D591" Ref="R8"  Part="1" 
F 0 "R8" H 2120 4196 50  0000 L CNN
F 1 "255" H 2120 4105 50  0000 L CNN
F 2 "Resistors_SMD:R_0603" V 1980 4150 50  0001 C CNN
F 3 "~" H 2050 4150 50  0001 C CNN
F 4 "P255DBCT-ND" H 2050 4150 50  0001 C CNN "Digikey Part #"
	1    2050 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 3800 1900 3800
Wire Wire Line
	1900 3800 1900 3700
Wire Wire Line
	1900 3700 2050 3700
Connection ~ 2050 4000
Wire Wire Line
	1850 4000 2050 4000
$Comp
L power:GND #PWR0106
U 1 1 5D270CAA
P 1150 3900
F 0 "#PWR0106" H 1150 3650 50  0001 C CNN
F 1 "GND" H 1155 3727 50  0000 C CNN
F 2 "" H 1150 3900 50  0001 C CNN
F 3 "" H 1150 3900 50  0001 C CNN
	1    1150 3900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5D270D2D
P 2050 4300
F 0 "#PWR0107" H 2050 4050 50  0001 C CNN
F 1 "GND" H 2055 4127 50  0000 C CNN
F 2 "" H 2050 4300 50  0001 C CNN
F 3 "" H 2050 4300 50  0001 C CNN
	1    2050 4300
	1    0    0    -1  
$EndComp
Text GLabel 6500 3000 0    50   Input ~ 0
VDD
Wire Wire Line
	6500 3000 6550 3000
Wire Wire Line
	6550 3000 6550 2900
Text GLabel 7450 3750 2    50   Input ~ 0
VDD
$Comp
L Device:R R?
U 1 1 5D27801E
P 2050 3550
AR Path="/5D25C1D5/5D27801E" Ref="R?"  Part="1" 
AR Path="/5D27801E" Ref="R2"  Part="1" 
F 0 "R2" H 2120 3596 50  0000 L CNN
F 1 "402" H 2120 3505 50  0000 L CNN
F 2 "Resistors_SMD:R_0603" V 1980 3550 50  0001 C CNN
F 3 "~" H 2050 3550 50  0001 C CNN
F 4 "YAG2658CT-ND" H 2050 3550 50  0001 C CNN "Digikey Part #"
	1    2050 3550
	1    0    0    -1  
$EndComp
Connection ~ 2050 3700
Text GLabel 2400 3700 2    50   Input ~ 0
VREF
Text Notes 1300 3300 0    50   ~ 0
Rs limits current consumption of reference
Text Notes 2250 4050 0    50   ~ 0
Sets VREF
Text GLabel 8350 4250 2    50   Input ~ 0
VREF
$Comp
L Device:R R?
U 1 1 5D27F33B
P 10100 3800
AR Path="/5D25D1A7/5D27F33B" Ref="R?"  Part="1" 
AR Path="/5D27E62D/5D27F33B" Ref="R?"  Part="1" 
AR Path="/5D27F33B" Ref="R3"  Part="1" 
F 0 "R3" H 10170 3846 50  0000 L CNN
F 1 "10k" H 10170 3755 50  0000 L CNN
F 2 "Resistors_SMD:R_0603" V 10030 3800 50  0001 C CNN
F 3 "~" H 10100 3800 50  0001 C CNN
	1    10100 3800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5D280385
P 9800 3900
AR Path="/5D25D1A7/5D280385" Ref="R?"  Part="1" 
AR Path="/5D27E62D/5D280385" Ref="R?"  Part="1" 
AR Path="/5D280385" Ref="R5"  Part="1" 
F 0 "R5" H 9870 3946 50  0000 L CNN
F 1 "10k" H 9870 3855 50  0000 L CNN
F 2 "Resistors_SMD:R_0603" V 9730 3900 50  0001 C CNN
F 3 "~" H 9800 3900 50  0001 C CNN
	1    9800 3900
	1    0    0    -1  
$EndComp
Text GLabel 9700 3600 0    50   Input ~ 0
VDD
Wire Wire Line
	10100 3650 10100 3600
Wire Wire Line
	10100 3600 9800 3600
Connection ~ 9800 3600
Wire Wire Line
	9800 3600 9800 3750
Text GLabel 9300 3950 0    50   Input ~ 0
SDA
Text GLabel 9300 4050 0    50   Input ~ 0
SCL
Text GLabel 8100 4050 2    50   Input ~ 0
SDA
Text GLabel 8100 4150 2    50   Input ~ 0
SCL
Wire Wire Line
	8000 4050 8100 4050
Text GLabel 5300 4200 2    50   Input ~ 0
SDA
Text GLabel 5300 4300 2    50   Input ~ 0
SCL
Wire Wire Line
	5150 4200 5300 4200
Text GLabel 4450 4200 0    50   Input ~ 0
VDD
Wire Wire Line
	4450 4200 4600 4200
Wire Wire Line
	7450 3750 7400 3750
Wire Wire Line
	7400 3750 7400 4050
$Sheet
S 4600 4100 550  500 
U 5D2A1FF9
F0 "sheet5D2A1FF0" 50
F1 "file5D25CB02.sch" 50
F2 "V+" I L 4600 4300 50 
F3 "V-" I L 4600 4400 50 
F4 "VDD" I L 4600 4200 50 
F5 "SDA" I R 5150 4200 50 
F6 "SCL" I R 5150 4300 50 
F7 "VSS" I L 4600 4500 50 
F8 "VREF" I R 5150 4400 50 
$EndSheet
Text GLabel 5250 4400 2    50   Input ~ 0
VREF
Wire Wire Line
	5150 4400 5250 4400
Text Notes 9100 3450 0    50   ~ 0
Series resistors on i2c lines restrict current\ntransients and filter high frequencies
$Sheet
S 4300 3350 550  500 
U 5D25CB0A
F0 "CurrentSense" 50
F1 "file5D25CB09.sch" 50
F2 "VREF" I R 4850 3750 50 
F3 "VDD" I L 4300 3550 50 
F4 "VSS" I L 4300 3650 50 
F5 "SDA" I R 4850 3550 50 
F6 "SCL" I R 4850 3650 50 
F7 "IN+" I L 4300 3450 50 
F8 "IN-" I R 4850 3450 50 
$EndSheet
Wire Wire Line
	3700 4400 3700 4500
Wire Wire Line
	3700 4500 3700 4800
Wire Wire Line
	5150 4300 5300 4300
Text GLabel 5750 4200 0    50   Input ~ 0
SDA
Text GLabel 5750 4300 0    50   Input ~ 0
SCL
Wire Wire Line
	5900 4200 5750 4200
Text GLabel 5800 4400 0    50   Input ~ 0
VREF
Wire Wire Line
	5900 4400 5800 4400
Wire Wire Line
	5900 4300 5750 4300
Text GLabel 6850 4200 2    50   Input ~ 0
VDD
Text GLabel 5000 3550 2    50   Input ~ 0
SDA
Text GLabel 5000 3650 2    50   Input ~ 0
SCL
Wire Wire Line
	4850 3550 5000 3550
Text GLabel 4950 3750 2    50   Input ~ 0
VREF
Wire Wire Line
	4850 3650 5000 3650
Wire Wire Line
	4850 3750 4950 3750
Text GLabel 3850 3550 0    50   Input ~ 0
VDD
Wire Wire Line
	4150 3550 4300 3550
$Comp
L power:GND #PWR0108
U 1 1 5D2BE62C
P 4150 3700
F 0 "#PWR0108" H 4150 3450 50  0001 C CNN
F 1 "GND" H 4155 3527 50  0000 C CNN
F 2 "" H 4150 3700 50  0001 C CNN
F 3 "" H 4150 3700 50  0001 C CNN
	1    4150 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 3650 4150 3700
Wire Wire Line
	3700 4400 4600 4400
Wire Wire Line
	3700 4500 4600 4500
Wire Wire Line
	3500 3450 3650 3450
Wire Wire Line
	3500 3550 3500 4400
Wire Wire Line
	3650 3450 3650 4300
$Comp
L Connector:Conn_01x02_Male J3
U 1 1 5D2E6060
P 10550 4050
F 0 "J3" H 10656 4228 50  0000 C CNN
F 1 "MCU I2C" H 10656 4137 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 10550 4050 50  0001 C CNN
F 3 "~" H 10550 4050 50  0001 C CNN
	1    10550 4050
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Male J4
U 1 1 5D2E94DE
P 10550 4500
F 0 "J4" H 10656 4678 50  0000 C CNN
F 1 "MCU PWR" H 10656 4587 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 10550 4500 50  0001 C CNN
F 3 "~" H 10550 4500 50  0001 C CNN
	1    10550 4500
	-1   0    0    1   
$EndComp
Wire Wire Line
	10150 4600 10150 4500
Wire Wire Line
	10150 4500 10350 4500
Text GLabel 10150 4400 0    50   Input ~ 0
VDD
Wire Wire Line
	10150 4400 10350 4400
Wire Wire Line
	9700 3600 9800 3600
Wire Wire Line
	9800 4050 10350 4050
Wire Wire Line
	10100 3950 10350 3950
$Comp
L Device:R R?
U 1 1 5D2F2095
P 9500 3950
AR Path="/5D25D1A7/5D2F2095" Ref="R?"  Part="1" 
AR Path="/5D27E62D/5D2F2095" Ref="R?"  Part="1" 
AR Path="/5D2F2095" Ref="R6"  Part="1" 
F 0 "R6" H 9300 4000 50  0000 L CNN
F 1 "330" H 9250 3900 50  0000 L CNN
F 2 "Resistors_SMD:R_0603" V 9430 3950 50  0001 C CNN
F 3 "~" H 9500 3950 50  0001 C CNN
	1    9500 3950
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5D2F20E0
P 9500 4050
AR Path="/5D25D1A7/5D2F20E0" Ref="R?"  Part="1" 
AR Path="/5D27E62D/5D2F20E0" Ref="R?"  Part="1" 
AR Path="/5D2F20E0" Ref="R7"  Part="1" 
F 0 "R7" H 9600 4100 50  0000 L CNN
F 1 "330" H 9600 4000 50  0000 L CNN
F 2 "Resistors_SMD:R_0603" V 9430 4050 50  0001 C CNN
F 3 "~" H 9500 4050 50  0001 C CNN
	1    9500 4050
	0    1    1    0   
$EndComp
Connection ~ 10100 3950
Wire Wire Line
	9650 3950 10100 3950
Wire Wire Line
	9650 4050 9800 4050
Connection ~ 9800 4050
Wire Wire Line
	9350 4050 9300 4050
Wire Wire Line
	9300 3950 9350 3950
Wire Wire Line
	7300 5000 7200 5000
Wire Wire Line
	7300 5000 7300 5050
Wire Wire Line
	7450 4150 7300 4150
Connection ~ 7300 4150
Wire Wire Line
	7100 4300 7100 4150
Wire Wire Line
	7100 4150 7300 4150
Wire Wire Line
	7300 4350 7450 4350
Wire Notes Line
	2700 4550 2700 3650
Wire Notes Line
	2700 3650 1850 3650
Wire Notes Line
	1850 3650 1850 4550
Wire Notes Line
	1850 4550 2700 4550
$Comp
L battery_emulator-rescue:USB_PORT-SFUSat-battery_emulator P1
U 1 1 5D376ABA
P 6150 1200
F 0 "P1" V 5713 1194 60  0000 C CNN
F 1 "USB_PORT-SFUSat" V 5819 1194 60  0000 C CNN
F 2 "battery_emulator:USB_Micro_B_Female_10118194-0001LF" H 6150 1200 60  0001 C CNN
F 3 "" H 6150 1200 60  0001 C CNN
F 4 "609-4618-1-ND" V 6150 1200 50  0001 C CNN "Digikey Part #"
	1    6150 1200
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5D37C97F
P 6350 1450
F 0 "#PWR0109" H 6350 1200 50  0001 C CNN
F 1 "GND" H 6355 1277 50  0000 C CNN
F 2 "" H 6350 1450 50  0001 C CNN
F 3 "" H 6350 1450 50  0001 C CNN
	1    6350 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 1050 6550 1050
Wire Wire Line
	7450 4250 7200 4250
$Comp
L Device:R R16
U 1 1 5D3880C3
P 4550 3150
F 0 "R16" V 4343 3150 50  0000 C CNN
F 1 "SENSE" V 4434 3150 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 4480 3150 50  0001 C CNN
F 3 "~" H 4550 3150 50  0001 C CNN
	1    4550 3150
	0    1    1    0   
$EndComp
Wire Wire Line
	4150 3450 4150 3150
Wire Wire Line
	4150 3150 4400 3150
Connection ~ 3650 3450
Wire Wire Line
	4700 3150 5200 3150
Wire Wire Line
	5200 3150 5200 3450
Wire Wire Line
	5200 3450 6400 3450
Wire Wire Line
	4850 3450 5200 3450
Connection ~ 5200 3450
Wire Wire Line
	4150 3450 4300 3450
Wire Wire Line
	7300 4350 7300 5000
Connection ~ 7300 5000
Wire Wire Line
	7200 4250 7200 4400
Wire Wire Line
	6450 4300 6550 4300
Wire Wire Line
	6450 4400 7200 4400
Connection ~ 7200 4400
Wire Wire Line
	7200 4400 7200 4500
Wire Wire Line
	6450 4500 7200 4500
Connection ~ 7200 4500
Wire Wire Line
	7200 4500 7200 5000
Wire Wire Line
	2050 3400 2000 3400
Text GLabel 2000 3400 0    50   Input ~ 0
VDD
$Sheet
S 7450 3950 550  500 
U 5D25C1D5
F0 "VoltageSource" 50
F1 "file5D25C1D4.sch" 50
F2 "VOUT" I L 7450 4150 50 
F3 "SDA" I R 8000 4050 50 
F4 "SCl" I R 8000 4150 50 
F5 "VDD" I L 7450 4050 50 
F6 "VSS" I L 7450 4250 50 
F7 "VREF" I R 8000 4250 50 
F8 "PGND" I L 7450 4350 50 
$EndSheet
Wire Wire Line
	8000 4150 8100 4150
Wire Wire Line
	8000 4250 8050 4250
$Sheet
S 5900 4100 550  500 
U 5D2AE93E
F0 "sheet5D2AE932" 50
F1 "file5D25CB02.sch" 50
F2 "V+" I R 6450 4300 50 
F3 "V-" I R 6450 4400 50 
F4 "VDD" I R 6450 4200 50 
F5 "SDA" I L 5900 4200 50 
F6 "SCL" I L 5900 4300 50 
F7 "VSS" I R 6450 4500 50 
F8 "VREF" I L 5900 4400 50 
$EndSheet
Wire Wire Line
	6450 4200 6850 4200
$Comp
L Device:R R9
U 1 1 5D28312B
P 4000 3550
F 0 "R9" V 3793 3550 50  0000 C CNN
F 1 "0" V 3884 3550 50  0000 C CNN
F 2 "Resistor_SMD:R_2010_5025Metric" V 3930 3550 50  0001 C CNN
F 3 "~" H 4000 3550 50  0001 C CNN
	1    4000 3550
	0    1    1    0   
$EndComp
Wire Wire Line
	3700 4400 3500 4400
Connection ~ 3700 4400
Wire Wire Line
	3650 4300 4600 4300
Wire Wire Line
	4150 3650 4300 3650
Wire Wire Line
	3650 3450 4150 3450
Connection ~ 4150 3450
$Comp
L Device:R R17
U 1 1 5D28D06D
P 8200 4250
F 0 "R17" V 8400 4250 50  0000 C CNN
F 1 "0" V 8300 4250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8130 4250 50  0001 C CNN
F 3 "~" H 8200 4250 50  0001 C CNN
	1    8200 4250
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP2
U 1 1 5D292D25
P 2350 3400
F 0 "TP2" H 2300 3600 50  0000 L CNN
F 1 "TestPoint" H 2200 3700 50  0000 L CNN
F 2 "TestPoint:TestPoint_THTPad_2.0x2.0mm_Drill1.0mm" H 2550 3400 50  0001 C CNN
F 3 "~" H 2550 3400 50  0001 C CNN
	1    2350 3400
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP1
U 1 1 5D292EE5
P 1150 3900
F 0 "TP1" H 1000 4050 50  0000 L CNN
F 1 "TestPoint" H 1050 3950 50  0000 R CNN
F 2 "TestPoint:TestPoint_THTPad_2.0x2.0mm_Drill1.0mm" H 1350 3900 50  0001 C CNN
F 3 "~" H 1350 3900 50  0001 C CNN
	1    1150 3900
	1    0    0    -1  
$EndComp
Connection ~ 1150 3900
$Comp
L Connector:TestPoint TP3
U 1 1 5D2964C3
P 6550 3000
F 0 "TP3" V 6504 3188 50  0000 L CNN
F 1 "TestPoint" V 6595 3188 50  0000 L CNN
F 2 "TestPoint:TestPoint_THTPad_2.0x2.0mm_Drill1.0mm" H 6750 3000 50  0001 C CNN
F 3 "~" H 6750 3000 50  0001 C CNN
	1    6550 3000
	0    1    1    0   
$EndComp
Connection ~ 6550 3000
Wire Wire Line
	7300 3450 7300 4150
Wire Wire Line
	2050 3700 2350 3700
Connection ~ 2350 3700
Wire Wire Line
	2350 3700 2400 3700
$Comp
L Device:R R18
U 1 1 5D2943B9
P 2350 3550
F 0 "R18" H 2420 3596 50  0000 L CNN
F 1 "0" H 2420 3505 50  0000 L CNN
F 2 "Resistor_SMD:R_2010_5025Metric" V 2280 3550 50  0001 C CNN
F 3 "~" H 2350 3550 50  0001 C CNN
	1    2350 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R19
U 1 1 5D29713D
P 6700 1050
F 0 "R19" V 6493 1050 50  0000 C CNN
F 1 "R" V 6584 1050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6630 1050 50  0001 C CNN
F 3 "~" H 6700 1050 50  0001 C CNN
	1    6700 1050
	0    1    1    0   
$EndComp
$Comp
L Device:LED D3
U 1 1 5D2971E9
P 7000 1050
F 0 "D3" H 6992 795 50  0000 C CNN
F 1 "LED" H 6992 886 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 7000 1050 50  0001 C CNN
F 3 "~" H 7000 1050 50  0001 C CNN
	1    7000 1050
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 5D29729B
P 7150 1050
F 0 "#PWR0110" H 7150 800 50  0001 C CNN
F 1 "GND" V 7155 922 50  0000 R CNN
F 2 "" H 7150 1050 50  0001 C CNN
F 3 "" H 7150 1050 50  0001 C CNN
	1    7150 1050
	0    -1   -1   0   
$EndComp
Connection ~ 6550 1050
$Comp
L Device:R R20
U 1 1 5D29E3EC
P 6400 3700
F 0 "R20" V 6193 3700 50  0000 C CNN
F 1 "R" V 6284 3700 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6330 3700 50  0001 C CNN
F 3 "~" H 6400 3700 50  0001 C CNN
	1    6400 3700
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D2
U 1 1 5D29E3F3
P 6100 3700
F 0 "D2" H 6091 3916 50  0000 C CNN
F 1 "LED" H 6091 3825 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 6100 3700 50  0001 C CNN
F 3 "~" H 6100 3700 50  0001 C CNN
	1    6100 3700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 5D29E3FA
P 5950 3700
F 0 "#PWR0111" H 5950 3450 50  0001 C CNN
F 1 "GND" V 5955 3572 50  0000 R CNN
F 2 "" H 5950 3700 50  0001 C CNN
F 3 "" H 5950 3700 50  0001 C CNN
	1    5950 3700
	0    1    1    0   
$EndComp
Wire Wire Line
	6550 3700 6550 4300
Connection ~ 6550 4300
Wire Wire Line
	6550 4300 7100 4300
NoConn ~ 6550 4200
$Comp
L Device:Q_PMOS_GSD Q1
U 1 1 5D3F53B6
P 6650 2700
F 0 "Q1" H 6856 2746 50  0000 L CNN
F 1 "Reverse Protection" H 6856 2655 50  0000 L CNN
F 2 "battery_emulator:AO3401A" H 6850 2800 50  0001 C CNN
F 3 "~" H 6650 2700 50  0001 C CNN
F 4 "785-1001-1-ND" H 6650 2700 50  0001 C CNN "Digikey Part #"
	1    6650 2700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6550 1050 6550 2200
Wire Wire Line
	6550 2200 7300 2200
Wire Wire Line
	6850 2700 7300 2700
$EndSCHEMATC
