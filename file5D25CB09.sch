EESchema Schematic File Version 4
LIBS:battery_emulator-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 5000 2950 0    50   ~ 0
Current sense or coulomb counter?
Text Notes 3750 4650 0    50   ~ 0
Coulomb counter has max 50mV sense differential. Need less than 100 mOhm resistor for full current range
Text Notes 3800 4750 0    50   Italic 10
What is the sense voltage resolution?
Text Notes 3850 4900 0    50   ~ 0
How to get instantaneous current?
Text Notes 3650 4550 0    50   ~ 0
LTC4150
Text Notes 5800 4900 0    50   ~ 0
ADC to minimize series resistance
Text Notes 3300 5600 0    50   ~ 0
100mV range amplified by 50 to 5V range interpreted by 12-bit ADC to give 1.7mA resolution across 6.8A range using 14.7mOhm sense resistor\n\n(Rsense*Isense*Gain/(Vref/4096))*(Imax/4096) = Idigital\n\n%error on rsense, gain, and vref propagate through to Idigital additively\n\nreduce absolute error by decreasing imax, increasing tolerance of rsense, choosing stable vref (not USB)
Text Notes 5000 2200 0    50   ~ 0
http://www.ti.com/lit/ds/symlink/ina199.pdf
Text Notes 3700 6050 0    50   ~ 0
ADC resolution chosen to give maximum resolution with 0 error. However, if\nsignal error exceeds resolution, not much point to higher resolution unless \nthe error can be calibrated out (ie Rsense error).
Text Notes 2800 6600 0    50   ~ 0
4V stable Vref (under USB range), 5A max (around 1.5C for most batteries), gain\n of 50 yields 80mV max differential therefore 16mOhm sense resistor with tightest \nprecision available.
Text Notes 2050 6900 0    50   ~ 0
1% gain error, 1% vref error, and 0.1% resistor error yields 2.1% error and around 105mA offset at 5A, 7.14mA at 340mA\n0.315mA at 15mA, error is less than minimum bucket of 1.22mA with 12 bits
Text Notes 3300 2400 0    50   ~ 0
https://www.digikey.ca/product-detail/en/microchip-technology/MCP3221A5T-I-OT/MCP3221A5T-I-OTCT-ND/529836
$Comp
L battery_emulator-rescue:MAX11644-battery_emulator U5
U 1 1 5D268B3B
P 6650 3700
F 0 "U5" H 6650 4115 50  0000 C CNN
F 1 "MAX11644" H 6650 4024 50  0000 C CNN
F 2 "battery_emulator:uMAX_8" H 6550 4000 50  0001 C CNN
F 3 "" H 6650 3700 50  0001 C CNN
F 4 "MAX11644EUA+-ND" H 6750 4200 50  0001 C CNN "Digikey Part #"
	1    6650 3700
	1    0    0    -1  
$EndComp
$Comp
L battery_emulator-rescue:INA199-battery_emulator U?
U 1 1 5D2A5F36
P 5250 3750
AR Path="/5D2A5F36" Ref="U?"  Part="1" 
AR Path="/5D25CB0A/5D2A5F36" Ref="U6"  Part="1" 
F 0 "U6" H 5250 4115 50  0000 C CNN
F 1 "INA199" H 5250 4024 50  0000 C CNN
F 2 "battery_emulator:SC70-6" H 5150 4000 50  0001 C CNN
F 3 "" H 5250 3750 50  0001 C CNN
F 4 "296-27329-1-ND" H 5350 4200 50  0001 C CNN "Digikey Part #"
	1    5250 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 3650 6200 3650
Wire Wire Line
	6200 3550 5900 3550
Wire Wire Line
	5900 3550 5900 3250
Wire Wire Line
	5900 3250 4450 3250
Wire Wire Line
	4450 3250 4450 3750
Wire Wire Line
	4450 3750 4850 3750
Text HLabel 4750 3650 0    50   Input ~ 0
VREF
Wire Wire Line
	4750 3650 4850 3650
Text HLabel 6100 3850 0    50   Input ~ 0
VREF
Wire Wire Line
	6100 3850 6200 3850
Text HLabel 7200 3550 2    50   Input ~ 0
VDD
Wire Wire Line
	7100 3550 7200 3550
Text HLabel 7200 3650 2    50   Input ~ 0
VSS
Wire Wire Line
	7100 3650 7200 3650
Text HLabel 4350 3750 0    50   Input ~ 0
VSS
Wire Wire Line
	4350 3750 4450 3750
Connection ~ 4450 3750
Text HLabel 7200 3750 2    50   Input ~ 0
SDA
Text HLabel 7200 3850 2    50   Input ~ 0
SCL
Wire Wire Line
	7100 3750 7200 3750
Wire Wire Line
	7100 3850 7200 3850
Text HLabel 4750 3850 0    50   Input ~ 0
VDD
Wire Wire Line
	4750 3850 4850 3850
Wire Wire Line
	5650 3850 5700 3850
Wire Wire Line
	5700 3850 5700 4300
Wire Wire Line
	5650 3750 5800 3750
Wire Wire Line
	5800 3750 5800 4300
Text Notes 5400 950  0    50   ~ 10
NEED TO SPLIT THE GROUNDS?
Text HLabel 5700 4300 3    50   Input ~ 0
IN+
Text HLabel 5800 4300 3    50   Input ~ 0
IN-
$EndSCHEMATC
