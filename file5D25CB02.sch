EESchema Schematic File Version 4
LIBS:battery_emulator-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 8350 6000 0    50   ~ 0
ADC?
Text Notes 7950 6100 0    50   ~ 0
http://ww1.microchip.com/downloads/en/DeviceDoc/20001805C.pdf
Text Notes 8150 5950 0    50   ~ 0
Onboard ADC w/ voltage ref?
Text Notes 7900 6200 0    50   ~ 0
16-bit: http://ww1.microchip.com/downloads/en/DeviceDoc/22072b.pdf
$Comp
L battery_emulator-rescue:LTC2453-battery_emulator U?
U 1 1 5D268A29
P 5750 3750
AR Path="/5D25D1A7/5D268A29" Ref="U?"  Part="1" 
AR Path="/5D2AE93E/5D268A29" Ref="U7"  Part="1" 
AR Path="/5D2A1FF9/5D268A29" Ref="U4"  Part="1" 
F 0 "U7" H 5750 4165 50  0000 C CNN
F 1 "LTC2453" H 5750 4074 50  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-23-8" H 5650 4050 50  0001 C CNN
F 3 "" H 5750 3750 50  0001 C CNN
F 4 "LTC2453CTS8#TRMPBFCT-ND" H 5850 4250 50  0001 C CNN "Digikey Part #"
	1    5750 3750
	1    0    0    -1  
$EndComp
Text HLabel 6300 3700 2    50   Input ~ 0
SCL
Text HLabel 6300 3600 2    50   Input ~ 0
SDA
Text HLabel 6950 3800 2    50   Input ~ 0
V+
Text HLabel 6300 3900 2    50   Input ~ 0
V-
Text HLabel 5050 3900 0    50   Input ~ 0
VDD
Text HLabel 5200 3600 0    50   Input ~ 0
VSS
Text HLabel 5200 3800 0    50   Input ~ 0
VREF
Wire Wire Line
	5300 3700 5250 3700
Wire Wire Line
	5250 3700 5250 3600
Wire Wire Line
	5250 3600 5200 3600
Wire Wire Line
	5300 3600 5250 3600
Connection ~ 5250 3600
Wire Wire Line
	5300 3800 5200 3800
Wire Wire Line
	6200 3700 6300 3700
Wire Wire Line
	6200 3600 6300 3600
$Comp
L Device:C C?
U 1 1 5D29E44A
P 5150 4050
AR Path="/5D25D1A7/5D29E44A" Ref="C?"  Part="1" 
AR Path="/5D27E62D/5D29E44A" Ref="C?"  Part="1" 
AR Path="/5D2AE93E/5D29E44A" Ref="C13"  Part="1" 
AR Path="/5D2A1FF9/5D29E44A" Ref="C10"  Part="1" 
F 0 "C13" H 5265 4096 50  0000 L CNN
F 1 "0.1uF" H 5265 4005 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 5188 3900 50  0001 C CNN
F 3 "~" H 5150 4050 50  0001 C CNN
	1    5150 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 3900 5150 3900
Wire Wire Line
	5150 3900 5300 3900
Connection ~ 5150 3900
Text HLabel 5100 4250 0    50   Input ~ 0
VSS
Wire Wire Line
	5100 4250 5150 4250
Wire Wire Line
	5150 4250 5150 4200
Wire Wire Line
	6200 3800 6750 3800
$Comp
L Device:C C?
U 1 1 5D29FC7C
P 6750 4050
AR Path="/5D25D1A7/5D29FC7C" Ref="C?"  Part="1" 
AR Path="/5D27E62D/5D29FC7C" Ref="C?"  Part="1" 
AR Path="/5D2AE93E/5D29FC7C" Ref="C15"  Part="1" 
AR Path="/5D2A1FF9/5D29FC7C" Ref="C12"  Part="1" 
F 0 "C15" H 6865 4096 50  0000 L CNN
F 1 "0.1uF" H 6865 4005 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 6788 3900 50  0001 C CNN
F 3 "~" H 6750 4050 50  0001 C CNN
	1    6750 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 4250 6750 4200
Wire Wire Line
	6750 3900 6750 3800
Connection ~ 6750 3800
Wire Wire Line
	6750 3800 6950 3800
Connection ~ 5150 4250
Wire Wire Line
	6200 3900 6300 3900
Wire Wire Line
	5150 4250 6750 4250
$EndSCHEMATC
